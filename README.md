```
Athira (ex-Motorola), Souranil (ex-ThoughtWorks) and 200+ developers have solved Geektrust coding challenges to find great jobs over the last 4 years
```
##### ✴ Get feedback on your coding skills. Detailed, handcrafted feedback on your code.

##### ✴ Get priority and be treated as a premium candidate to directly connect with decision makers at companies.

##### ✴ Get membership and win an exclusive Geektrust DEVELOPER t-shirt.

```
Over 2000 developers from the best companies in the world have trusted us with their code. And we don’t look just at the output, but how you get it is
more important. We care about how well modelled your code is, how readable, extensible, well tested it is. Have questions on the challenges or our
evaluation? Ping us on the Geektrust Slack channel or mail us at devs@geektrust.in saying “Add to slack”.
```

# Getting started

##### 1. Getting the output right is important, but more important is clean code and how well designed your code is.

##### You should absolutely read this post on what we look for in your code, and how to get started with the

##### coding challenge.

##### 2. See our evaluation parameters here and the badges to earn here.

##### 3. We expect a command line app. So no web apps will be considered for evaluation. You don’t need data

##### stores either.


# Problem Context

##### Our story is set in the planet of Lengaburu......in the distant, distant

##### galaxy of Tara B. And our protagonists are King Shan, Queen Anga &

##### their family.

##### King Shan is the emperor of Lengaburu and has been ruling the

##### planet for the last 350 years (they have long lives in Lengaburu, you

##### see!). Let’s write some code to get to know the family.

##### This coding problem is for backend and fullstack developers.


# Family Tree


# Meet The Family

```
Write code to model out the King Shan family tree so that:
```
- Given a ‘name’ and a ‘relationship’, you should output the people corresponding to the relationship in the order in
    which they were added to the family tree. Assume the names of the family members are unique.
- You should be able to add a child to any family in the tree through the mother.

```
Simple, right? But remember.. our evaluation is based not only on getting the right output, but on how you've written your code.
```
```
Relationships To Handle
There are many relations that could exist but at a minimum, your code needs to handle these relationships.
```
**Relationships Paternal-Uncle Maternal-Uncle Paternal-Aunt Maternal-Aunt Sister-In-Law Brother-In-Law Son Daughter Siblings**

```
Definition Father’s brothers Mother’s brothers Father’s sisters Mother’s sisters Wives of siblingsSpouse’s sisters,
```
```
Spouse’s brothers,
Husbands of siblings
```

# Sample Input/Output

```
ADD_CHILD ”Mother’s-Name" "Child's-Name"
"Gender"
```
```
GET_RELATIONSHIP ”Name” “Relationship”
```
##### Do initialise the existing family tree on program start. Your program should take the location to the

##### test file as parameter. Input needs to be read from a text file, and output should be printed to the

##### console. The test file will contain only commands to modify or verify the family tree.

```
ADD_CHILD Chitra Aria Female
GET_RELATIONSHIP Lavnya Maternal-Aunt
GET_RELATIONSHIP Aria Siblings
```
```
CHILD_ADDITION_SUCCEEDED
Aria
Jnki Ahit
```
```
Input format to add a child:
```
```
Input format to find the people belonging to a relationship:
```
```
Example test file:
```
```
”Name 1” “Name 2”... “Name N”
```
```
Output format on finding the relationship:
```
```
Output on finding the relationship:
```

##### More sample input output scenarios.

##### Please stick to the Sample input output format as shown. This is very important as we are automating the

##### correctness of the solution to give you a faster evaluation. You can find some sample input output files here.

```
ADD_CHILD Pjali Srutak Male
GET_RELATIONSHIP Pjali Son
```
```
PERSON_NOT_FOUND
PERSON_NOT_FOUND
```
```
Sample 1 Output on finding the relationship:
```
```
ADD_CHILD Asva Vani Female
GET_RELATIONSHIP Vasa Siblings
```
```
CHILD_ADDITION_FAILED
NONE
```
```
Sample 2 Output on finding the relationship:
```
```
GET_RELATIONSHIP Atya Sister-In-Law Satvy Krpi
```
```
Sample 3 Output on finding the relationship:
```
```
Pjali does not exist in the family tree
```
```
Asva is male, hence child addition failed
```

**Solution with build file**
We support only **Rake** build system. The rake file should import the main ruby file, which is the starting point of your application
and call the **main** method within the **default task**. Also create a **Gemfile** to add your dependencies, if any. The main file should
read the file path from the **ARGV** variable and then execute the program.

The solution will be run using the following commands. Read more.

```
bundle install
rake default <absolute_path_to_input_file>
```
```
Solution without build file
For a solution without build system, we want you to name your Main file as geektrust.rb. This is the file that will contain your main
method.
The solution will be run using the following commands. Read more.
ruby -W0 geektrust.rb <absolute_path_to_input_file>
```
#### Ruby - Instructions to Build & Execute


### Supported Language & Versions

##### Code submissions are run against a Linux virtualized instance.

##### Supported language and versions are below:

```
Language Supported versions Supported Tools
C# dotnet core 2.2, 3.1 dotnet
Go 1.12.x Go build tool
Java 1.8, 1.11 maven, gradle
Node.js v8.16.x, v10.16.x v12.6.x npm, yarn
Python 3.5,3.6,3.7,3.8 pip
Ruby 1.9.x, 2.2.x, 2.6.x rake
```
##### You can upload code in any version of Clojure, Scala, Kotlin, PHP, Erlang. We don't have automated tests

##### for these languages yet. So your evaluation will take longer than the others.


## Check list - submitting code

##### 1. Please compress the file before upload. We accept .zip, .rar, .gz and .gzip

##### 2. Name of the file should be the problem set name you are solving. For e.g. if you have solved Family

##### problem, please name your file ‘Family.zip’.

##### 3. Please upload only source files and do not include any libraries or executables or node_modules

##### folder.

##### 4. Usage of non-essential 3rd party libraries will affect your evaluation.

##### 5. Add a readme with how to get your code working, and how to test your code.

##### 6. Your solution will be downloaded & seen by companies you’re interested in. Hence we advise you to

##### provide a solution that will work on any system without any code changes/manual setup.


# what next?


