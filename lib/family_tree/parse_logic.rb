require_relative 'constants'
require_relative 'family'
module FamilyTree
  # ParseLogic Class
  class ParseLogic
    include Constants

    def initialize(command)
      @command = command
    end

    def call
      process_command
    end

    private

    attr_accessor :command

    def process_command
      puts command.join(" ")
      case command.first
      when Commands::ADD_FAMILY_HEAD
        Family.add_family_head(command[1], command[2])
      when Commands::ADD_SPOUSE
        Family.add_spouse(command[1], command[2], command[3])
      when Commands::ADD_CHILD
        Family.add_child(command[1], command[2], command[3])
      else
        sprintf Message::INVALID_COMMAND
      end
    end
  end
end