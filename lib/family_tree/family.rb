require_relative 'person'
module FamilyTree
	class Family
		
		@@family_list = nil
		
		class << self
			
			def add_family_head(name, gender)
				@@family_list = Person.new(name: name, gender: gender)
			end

			def family_tree
				@@family_list
			end

			def add_spouse(member_name, spouse_name, gender)
				person = @@family_list.find_spouse(member_name)
				spouse = Person.new(name: spouse_name, gender: gender, spouse: person)
				begin
					person.spouse = spouse
				rescue StandardError
					require 'pry'; binding.pry
				end
			end

			def add_child(parent_name, child_name, gender)
				person = @@family_list.find_parent(parent_name)
				child = Person.new(name: child_name, gender: gender, father: person.spouse, mother: person)
				person.children.push(child)
			end

		end 
	end
end






