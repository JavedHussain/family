require_relative 'constants'
require_relative 'parse_logic'

module FamilyTree
  class FileProcessor
    include Constants
    INPUT_LINE_SIZE = [3, 4].freeze

    def initialize(file_path:)
      @file_path = file_path
      @response = Array.new
    end

    def call
      parse_and_create_person
      binding.pry
      response
    end

    private

    attr_reader :file_path, :response

    def parse_and_create_person
      file = File.read(file_path)
      lines = split_by(file, "\n")

      lines.each do |line|
        line = split_by(line, ';')
        response.push(parse(line))
      end
    end

    def split_by(obj, splitter)
      obj.split(splitter.to_s)
    end

    def parse(obj)
      return Message::NONE unless INPUT_LINE_SIZE.include? obj.size

      ParseLogic.new(obj).call
    end
  end
end