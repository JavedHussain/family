module FamilyTree
  # Person class
  class Person
    def initialize(args={})
      @name     = args.dig(:name)
      @gender   = args.dig(:gender)
      @mother   = args.dig(:mother)
      @father   = args.dig(:father)
      @spouse   = args.dig(:spouse)
      @children = args.dig(:children) || []
    end

    def find_spouse(name)
      return self if self.name == name
      self.spouse.children.each do |child|
        child.find_spouse(name)
      end
    end

    def find_parent(name)
      return self.spouse if self.spouse.name == name
      self.spouse.children.each do |child|
        child.find_parent(name)
      end
    end

    attr_accessor :name, :gender, :mother, :father, :spouse, :children
  end
end