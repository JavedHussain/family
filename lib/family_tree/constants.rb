module Constants

  module Relationships
    PATERNAL_UNCLE = 'Paternal-Uncle'.freeze
    MATERNAL_UNCLE = 'Maternal-Uncle'.freeze
    PATERNAL_AUNT  = 'Paternal-Aunt'.freeze
    MATERNAL_AUNT  = 'Maternal-Aunt'.freeze
    SISTER_IN_LAW  = 'Sister-In-Law'.freeze
    BROTHER_IN_LAW = 'Brother-In-Law'.freeze
    SON            = 'Son'.freeze
    DAUGHTER       = 'Daughter'.freeze
    SIBLINGS       = 'Siblings'.freeze
  end

  module Message
    NOT_YET_IMPLEMENTED      = 'NOT YET IMPLEMENTED'.freeze
    PERSON_NOT_FOUND         = 'PERSON_NOT_FOUND'.freeze
    PROVIDE_VALID_RELATION   = 'PROVIDE VALID RELATION'.freeze
    CHILD_ADDITION_FAILED    = 'CHILD_ADDITION_FAILED'.freeze
    CHILD_ADDITION_SUCCEEDED = 'CHILD_ADDITION_SUCCEEDED'.freeze
    NONE                     = 'NONE'.freeze
    INVALID_COMMAND          = 'INVALID COMMAND!'.freeze
  end

  module Commands
    GET_RELATIONSHIP = 'GET_RELATIONSHIP'.freeze
    ADD_CHILD        = 'ADD_CHILD'.freeze
    ADD_FAMILY_HEAD  = 'ADD_FAMILY_HEAD'.freeze
    ADD_SPOUSE       = 'ADD_SPOUSE'.freeze
  end

end