require_relative 'family_tree/file_processor'
# Solution class
class Solution
  def initialize(args={})
    @file_path = args[:file_path]
    @default_file_path = args[:init_file_path]
  end

  def call
    seed_initial_family_tree
    process_input_file
  end

  private

  attr_reader :file_path, :default_file_path

  def seed_initial_family_tree
    call_processor(default_file_path)
  end

  def process_input_file
    call_processor(file_path)
  end

  def call_processor(file_path)
    FamilyTree::FileProcessor.new(file_path: file_path).call
  end
end